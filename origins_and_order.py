"""What do we know about Professor Boolean's past? It's mostly rumor
    and conjecture, but a few things are known to be true.

Mad Professor Boolean wasn't always a super villain. Early in his career,
he was an average paper pusher, working in an office with some very
backwards technology. One of his primary jobs was to carry date cards
between departments. One morning, he tripped over a unicycle and dropped
his date cards on the floor. He hit his head - and hit upon the idea of
breeding an army of zombie rabbits to do his bidding and manage simple tasks.
But that comes later. Before he could quit with an explosive YouTube video,
the professor had to get his cards back in order.

Aha! It seems he recorded the details of this life-changing event in
his diary. Let's try to reproduce his methods:

The goal is to get the date cards back in order. Each set of date cards
consists of 3 cards, each with a number written on it. When arranged in
some order, the numbers make up the representation of a date, in the
form month/day/year. However, sometimes multiple representations will be
possible. For example, if the date cards read 1, 1, 99 it could only
mean 01/01/99, but if the date cards read 2, 30, 3, it could mean any
one of 02/03/30, 03/02/30, or 03/30/02.

Write a function called answer(x, y, z) that takes as input the 3 numbers on
the date cards. You may assume that at least one valid representation of a
date can be constructed from the cards.

If there is only one valid representation, the function should return it as
a string, in the form MM/DD/YY. If there are multiple valid representations,
the function should return the string "Ambiguous." Each of x, y, z will be
between 1 to 99 inclusive. You may also assume that there are no leap years."""


def answer(x, y, z):

    months = {
            1: 31,
            2: 28,
            3: 31,
            4: 30,
            5: 31,
            6: 30,
            7: 31,
            8: 31,
            9: 30,
            10: 31,
            11: 30,
            12: 31
    }

    x = int(x)
    y = int(y)
    z = int(z)

    numbers = [x, y, z]

    # validate data
    if max(numbers) < 100 and min(numbers) > 0:
        # if all numbers are equal, and less than 12
        if x == y and x == z and max(numbers) <= 12:
                if len(str(x)) == 1:
                    return "0{}/0{}/0{}".format(x, y, z)
                else:
                    return "{}/{}/{}".format(x, y, z)
        # elif two of the numbers are equal and both less than or equal to 12 - 1/1/32
        elif x == y or x == z or y == z:
            # add all of the same numbers to a new list
            number_match_list = [numbers[index] for index, item in enumerate(numbers)
            if numbers.count(item) > 1 and item <= 12]
            # if there are numbers in the match list, check if third number is
            #greater than 32
            if number_match_list:
                for i in numbers:
                    if not i in number_match_list and i > 31:
                        if len(str(number_match_list[0])) == 1:
                            return "0{}/0{}/{}".format(number_match_list[0],
                                    number_match_list[1], i)
                        else:
                            return "{}/{}/{}".format(number_match_list[0],
                                    number_match_list[1], i)
            # elif two of the numbers are equal and both are greater than 12 and less
            # than 32
            elif x == y or x == z or y == z:
                number_match_list = [numbers[index] for index, item in enumerate(numbers)
                if numbers.count(item) > 1 and (item > 12 and item < 32)] # [small, large, large]
                # if there are numbers in the match list, check if third number is
                # less than or equal to 12
                if number_match_list:
                    for i in numbers: # <-- k/v pair analysis here (2/29/29 should not pass)
                        if not i in number_match_list and i in months.keys():
                            month_match = i
                            for k, v in months.items():
                                if month_match == k and number_match_list[0] <= v:
                                    if len(str(i)) == 1:
                                        date = "0{}/{}/{}".format(i, number_match_list[0],
                                                number_match_list[1])
                                        return date
                                    else:
                                        date = "{}/{}/{}".format(i, number_match_list[0],
                                                number_match_list[1])
                                        return date
        else:
            # else if all three numbers are different and meet case criteria:
            numbers = sorted(numbers) # (smallest, mid, largest)
            for k, v in months.items(): #compare first two numbers (month, day) to dictionary
                if k == numbers[0] and (v < numbers[1] or numbers[1] <= 12):
                    return "Ambiguous"
                elif k == numbers[0] and v >= numbers[1] and numbers[2] > v:
                    if numbers[0] < 10:
                        date = "0{}/{}/{}".format(numbers[0], numbers[1], numbers[2])
                        return date
                    else:
                        date =  "{}/{}/{}".format(numbers[0], numbers[1], numbers[2])
                        return date
    return "Ambiguous"


print(answer(1, 1, 1))
print(answer(19, 19, 3))
print(answer(3, 30, 3))
print(answer(1, 1, 3))
print(answer(99, 11, 13))
print(answer(12, 31, 30))
print(answer(2, 28, 29))
print(answer(2, 12, 12))
print(answer(1, 12, 32))
# - 01/12/99 - does not pass
